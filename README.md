# alef-project

## alef-project-api

### Development

1. Set up the environment variables
    Rename the `.env.example` file to `.env` and change the values if necessary

2. Install the dependencies

   ```bash
   docker-compose run --rm api bash
   yarn
   ```

3. Migrate the database

    ```bash
    npm run migrate
    ```

## alef-project-app

### Developemnt

1. Set up the environment variables
    Raname the .env.example file to `.env` and change the values if necessary

2. Install the dependencies

    ```bash
    docker-compose run --rm app bash
    yarn
    ```
