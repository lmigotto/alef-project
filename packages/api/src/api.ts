import * as express from 'express'
import * as cors from 'cors'

const api = express()

api.use(cors({ origin: process.env.CORS_ORIGINS || '*' }))

api.get('/', (req, res) => {
  res.json({ message: 'Hello, world!' })
})

api.use((err, req, res, next) => {
  res.status(500).json({ message: err.message, args: err.args })
})

export default api
