import api from './api'

const PORT = process.env.PORT || 3000
const HOST = process.env.HOST || '0.0.0.0'

const server = api.listen(PORT, () => {
  console.log(`API server is listening on http://${HOST}:${PORT}`)
})

server.once('close', () => api.emit('close'))

server.on('error', err => {
  console.log(err)
  if (err.code === 'EADDRINUSE') {
    server.emit('close')
  }
})

process.once('SIGTERM', () => {
  console.log('SIGTERM received')
  server.close()
})
