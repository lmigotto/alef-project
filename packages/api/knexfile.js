require('dotenv').config()

module.exports = {
  client: 'pg',
  connection: process.env.PG_ADMIN_CS,
  migrations: './migrations'
}
